# spring-boot-script

#### 介绍
spring boot启动脚本，包含window和linux脚本

#### 安装教程

1.  linux，配合config.properties和使用start.sh，主要修改config的配置项，start支持kill和spring-boot-actuator的shutdown接口关闭应用
2.  windows，直接使用脚本拼接参数，关闭启动后的窗口则关闭了应用
3.  欢迎投稿扩展
