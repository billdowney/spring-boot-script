#!/bin/bash
##spring boot启动脚本
##Writen by 2020-01-01

#需要启动的jar名称，相对于脚本目录
jarName='xxx.jar'
serverPort='8080'
serverContextPath='/billdowney-boot'
actuatorBasePath='/actuator'

outMsg='input param "start" or "stop"'

#spring boot需要拼接的参数
javaShellStr=''
#读取配置文件
while read line
do
	#k=`echo $line | awk -F '=' '{print $1}' | sed s/[[:space:]]//g`
	#v=`echo $line | awk -F '=' '{print $2}' | sed s/[[:space:]]//g`
	k=`echo ${line%%=*} | sed s/[[:space:]]//g`
	v=`echo ${line#*=} | sed s/[[:space:]]//g`
	
	#判断key是否为空字符串
	if [ -n "$k" ]; then
		#判断是否是注释行
		if [[ ! $k =~ ^'#'.* ]]; then
			case $k in
			'jar')
				jarName=$v
			;;
			'server.port')
				serverPort=$v
				javaShellStr="$javaShellStr --$k=$v"
			;;
			'server.servlet.context-path')
				serverContextPath=$v
				javaShellStr="$javaShellStr --$k=$v"
			;;
			'management.endpoints.web.base-path')
				actuatorBasePath=$v
				javaShellStr="$javaShellStr --$k=$v"
			;;
			*)
				javaShellStr="$javaShellStr --$k=$v"
			;;
			esac
		fi
	fi
done < config.properties
	
case $1 in
'start')
    echo -n "" > nohup.out
	nohup java -jar $jarName $javaShellStr &
	outMsg="starting"
    tail -f ./nohup.out
	;;
'stop')
	#使用kill命令关闭进程
	outMsg=`ps -efww | grep -i $USER | grep -i java | grep $jarName | awk '{print $2}' | xargs kill -9;`
	#使用spring boot actuator的shutdown接口关闭应用
	#outMsg=`curl http://127.0.0.1:$serverPort$serverContextPath$actuatorBasePath/shutdown -X POST`
	;;
'start-slience')
    echo -n "" > nohup.out
    nohup java -Dhttps.protocols=TLSv1,TLSv1.1,TLSv1.2,TLSv1.3 -jar $jarName $javaShellStr >/dev/null 2>&1 &
    outMsg="starting"
    ;;
esac
echo $outMsg
