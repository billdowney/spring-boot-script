﻿@echo on

rem 改变cmd窗口编码
chcp 65001

rem 需要运行的jar(此变量名不能修改)
set jarName=xxxx.jar
set excuteParams=-jar %jarName%

rem 以下是spring boot相关配置，会传入spring boot中，与spring boot配置同名，可以自行扩展
rem spring环境：dev、test、prod
set excuteParams=%excuteParams% --spring.profiles.active=prod

rem 服务器配置
set excuteParams=%excuteParams% --server.port=801
set excuteParams=%excuteParams% --server.servlet.context-path=/xxxx

rem 数据库配置
set excuteParams=%excuteParams% --spring.datasource.dynamic.datasource.master.url="jdbc:mysql://127.0.0.1:3306/xxxx?characterEncoding=UTF-8&useUnicode=true&useSSL=false&tinyInt1isBit=false&allowPublicKeyRetrieval=true&serverTimezone=Asia/Shanghai"
set excuteParams=%excuteParams% --spring.datasource.dynamic.datasource.master.username=xxxx
set excuteParams=%excuteParams% --spring.datasource.dynamic.datasource.master.password=xxxx
set excuteParams=%excuteParams% --spring.datasource.dynamic.datasource.master.driver-class-name=com.mysql.cj.jdbc.Driver

rem actuator配置
set excuteParams=%excuteParams% --management.server.address=127.0.0.1
set excuteParams=%excuteParams% --management.endpoint.shutdown.enabled=true
set excuteParams=%excuteParams% --management.endpoints.web.base-path=/actuator
set excuteParams=%excuteParams% --management.endpoints.web.exposure.include=shutdown,metrics,httptrace

rem redis配置
set excuteParams=%excuteParams% --spring.redis.database=10
set excuteParams=%excuteParams% --spring.redis.host=127.0.0.1
set excuteParams=%excuteParams% --spring.redis.port=6379

rem swagger2配置
rem swagger.enable=true

java %excuteParams%
pause